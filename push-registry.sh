#!/usr/bin/bash
# NAME
#	push-registry img
#
# SYNOPSIS
#	push a local image to the gitlab-registry
#
# DESCRIPTION
#	* push image in gitlab-registry.cern.ch/lhcb-readout40/software/p40-container.
#   * login to gitlab-registry.cern.ch is required
#
# HELP
#   $ push-registry image_name
#	$ push-registry image_name:tag
#
img=$1
gitlab=gitlab-registry.cern.ch
project=lhcb-readout40/software/p40-container

echo ""
echo "push localhost/"$img "to ${gitlab}"
echo ""

echo "please login to ${gitlab}"
buildah login ${gitlab}

echo ""
buildah push $img docker://${gitlab}/${project}/$img
exit
