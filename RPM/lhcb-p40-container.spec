Summary: script to run the p40-lli-devices stack using container
Name: lhcb-p40-container
Version:
Release:
Group: Development/System
Source: git clone https://gitlab.cern.ch/lhcb-readout40/software/p40-container.git
License: GPL
Packager: Luis Granado Cardoso <luis.granado@cern.ch>
Requires: buildah podman python3 skopeo

%description
The script to run the p40-lli-devices stack using container:
    - uses the root-less container podman
    - images are  located in the gitlab-registry.cern.ch
    - run several user cases fedd, stable, unstable, rep and git
Containers can be used for many different puprose, e.g. to test a new release
or run an old one without modifying the installation of the pc-server.

%define P40_CONTAINER_SOURCES $CI_PROJECT_DIR
%define INSTALLED_BASE /usr/local
%define RPM_BIN_BASE $RPM_BUILD_ROOT/%{INSTALLED_BASE}

# deactivate python bytecompiling
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

%prep
echo ""
echo "RPM_BUILD_DIR is $RPM_BUILD_DIR"
echo ""
rm -Rf $RPM_BUILD_DIR/*

#copie the sources got from git
cp -Rf %{P40_CONTAINER_SOURCES}/p40 $RPM_BUILD_DIR/

%build

%install
echo ""
echo "RPM_BUILD_ROOT is $RPM_BUILD_ROOT"
echo ""
rm -rf $RPM_BUILD_ROOT/*

mkdir -p $RPM_BUILD_ROOT/%{INSTALLED_BASE}/bin
cp $RPM_BUILD_DIR/*   $RPM_BUILD_ROOT/%{INSTALLED_BASE}/bin/

%clean
rm -rf $RPM_BUILD_ROOT

%post

%files
/%{INSTALLED_BASE}/bin/*

%postun

%changelog
