# build container images
#
#	make <key> [options]
#
# Possible values for key from help:
#
#   	make help
#
# ............................................................................
#
# variables to build images
#
gitlab := gitlab-registry.cern.ch
nullstring := 
project := lhcb-readout40/software/p40-container
push ?= $(nullstring)
release ?= latest
tag := $(subst .,,$(release))

# ............................................................................
#
# git metadata
#
gitdescribe := $(shell git describe --long)
gitrelease := $(subst -, , $(subst ., , $(gitdescribe)))
major := $(strip $(subst v,, $(word 1, $(gitrelease))))
minor := $(word 2, $(gitrelease))
patch := $(word 3, $(gitrelease))
commit := $(strip $(subst g,, $(word 5, $(gitrelease))))
branch := $(strip $(subst *,, $(shell git branch --contains $(commit))))

# ............................................................................
#
# variable to build the rpm
#
pkgname := "lhcb-p40-container"
version := $(major).$(minor).$(patch)
rpmrelease := $(word 4, $(gitrelease))
distdir := $(pkgname)-$(version)-$(rpmrelease)

rpmbuilddir := /tmp/rpmbuild-$(pkgname)
specfile := RPM/$(distdir).spec
tplspec := RPM/$(pkgname).spec

outdir := RPM/rpms

now := $(shell date +"%a %b %d %Y")
log1 := * $(now) Luis Granado Cardoso   <luis.granado@cern.ch> $(version)-$(rpmrelease)
log2 := - update at commit $(commit) from branch $(branch)

# ............................................................................

.PHONY: clean git help rpm

help:
	@echo ""
	@echo "    Usage: make <key> [option]"
	@echo ""
	@echo "    Positional arguments:"
	@echo ""
	@echo "        help             display this help message"
	@echo "        clean            remove all built images"
	@echo "        rpm              build the rpm lhcb-p40-container"
	@echo ""
	@echo "        conda            image with conda"
	@echo "        fedd             image to run the full stack installed at production site"
	@echo "        git              several images to run the full stack from git sources"
	@echo "        minidaq2         image to run minidaq2 setup via rpms (stable version)"
	@echo "        modules-reports  image with p40-modules-reports (git)"
	@echo "        stable           image to run the full stack via rpms (stable version)"
	@echo "        unstable         image to run the full stack via rpms (unstable version)"
	@echo ""
	@echo "        lli              image with p40-lli (git)"
	@echo "        lli-devices      image with p40-lli-devices (git)"	
	@echo "        top-git          image to run the full stack (git)"
	@echo ""
	@echo "    Optional arguments:"
	@echo ""
	@echo "         push     push the local image to the gitlab registry when not empty"
	@echo "                      > make stable push=true"
	@echo "                      > make stable release=6.6.0-0 push=true"
	@echo ""
	@echo "         release  rpm version to be used for lhcb-p40-lli-devices-tools."
	@echo "                  to be used with the stable key:"
	@echo "                      > make stable"
	@echo "                      > make stable release=6.6.0-0"
	@echo ""

# Implicit rule to build and push image
# The value of $@ is given by the positonal argument <key>
# Dockerfile are in img-$@ while image are named p40-$@
# The build context is set to p40-container
%:
	@echo ""
	@echo "build image p40-$@:$(tag)..."
	@echo ""
	
	@if [ "$(release)" = "latest" ]; then \
		buildah bud -t p40-$@ -f img-$@/Dockerfile .; \
	else \
		buildah bud -t p40-$@:$(tag) --build-arg RELEASE=$(release) -f img-$@/Dockerfile .; \
	fi

	@if [ $(push) > 0 ]; then \
		`pwd`/push-registry.sh p40-$@:$(tag); \
	fi
	
	@rm -f 0

clean:
	@echo ""
	@echo "remove all built images..."
	@echo ""
	-podman rmi p40-base 
	-podman rmi p40-conda 
	-podman rmi p40-fedd 
	-podman rmi p40-lli
	-podman rmi p40-lli-devices
	-podman rmi p40-minidaq2
	-podman rmi p40-modules-reports
	-podman rmi p40-rep
	-podman rmi p40-stable
	-podman rmi p40-top-git
	-podman rmi p40-unstable
	@echo ""

git: base conda lli lli-devices top-git

rpm:
	@echo ""
	@echo "build rpm lhcb-p40-container..."
	@echo "prepare the spec file $(specfile)..."
	@echo ""
	
	-rm $(specfile)
	
	sed  -e "s/Version:/& $(version)/" \
	     -e "s/Release:/& $(rpmrelease)/" \
	     -e '/%changelog/a\\n$(log1)\ \n$(log2)' $(tplspec) > $(specfile)    

	@echo ""
	@echo "build the rpm in $(rpmbuilddir)..."
	@echo ""

	rpmbuild -bb $(specfile)
	mkdir -p RPM/rpms
	cp -r $(HOME)/rpmbuild/RPMS/* $(outdir)/
	rm -rf $(HOME)/rpmbuild
	rm $(specfile)
	
