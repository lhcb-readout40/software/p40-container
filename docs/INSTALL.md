## Install p40-container

Install the rpm `lhcb-p40-container` which provides
the command `p40`:

        $ sudo yum install lhcb-p40-container

User cases can be run, access and reset via the command `p40` and
its subcommand:

        $ p40
        $ p40 <command> -h

The rpms `lhcb-p40-container` will install `podman` and its friends.

### Check user name space

Unfortunately, `podman` requires to allow user name spaces for existing user
accounts. This operation has to done once and requires `root` privilege:

* Edit `/etc/subuid` and `/etc/subgid` to add one line for each user:

            user1:100000:65536
            user1:200000:65536

* Enable user name spaces in `/proc/sys/user/max_user_namespaces`:

            $ sysctl user.max_user_namespaces=65536
            $ cat /proc/sys/user/max_user_namespaces

  Add in `/etc/sysctl.conf` the line:

            user.max_user_namespaces=65536

### Centos 7: check RPMs dependency

Check that the following RPMs are installed:

* `fuse`
* `shadow-utils`
* `kernel` at lest relase `3.10.0-1127`
* `slirp4netns`

### Centos 7: check the open file limit

Check the `open file limit`:

            $ ulimit -Hn

It has to be well above `4096`. It is know to work with `524288`.

In order to change the `open file limit` edit the file
`/etc/security/limits.conf` and add the line:

            @lhcb    hard  nofile 524288

This change will be effective for all members of the group `lhcb`.
