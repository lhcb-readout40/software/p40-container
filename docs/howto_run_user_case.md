## How to run a user case

The command `p40` allows to run any user case. Several cases can be
instantiated at the same time.

The sequence of operation is the following, for example for the `git` case:

        $ p40 start git
        $ p40 enter git

            ... the prompt becomes [roo@p40-git]
            ... the user top directory is root
            ... type command as usual

                [roo@p40-git] pcie40_id
                [roo@p40-git] pcie40_multi_functional
                [roo@p40-git] pcie40_calibrate_gbts

            ... type CTRL-d or exit

A user case can be stop and restart later on:

        $ p40 stop git
            ...
        $ p40 start git

A user case can be reset. The underlying structure of pod and volumes is
removed:

        $ p40 reset git

The list of instantiate user cases and their states is given by:

        $ p40 list

For the `stable` case a dedicated version has to be selected.
It is given by the rpm version, *e.g.* `6.7.2-0`:

        $ p40 start stable-672-0

or

        $ p40 start stable:672-0

To get more information on the `p40` command type:

        $ p40
        $ p40 <subcommand> -h
