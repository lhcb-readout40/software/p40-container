### How to list existing images on gitlab-registry and update local ones

Built images are located in the *top* registry `gitlab-registry.cern.ch`.
Required images are copied locally when the user start a case.

It is possible to see the list of image available in top registry
and their associations with user cases:

        $ p40 remote show

The above command shows the phasing between local images and those in the
top registry. From time to time, your local image are out of date with respect
to the top registry. Imges for a given cases can be updated:

        $ p40 remote update git
        $ p40 remote update stable:672-0

A prompt will ask the user to confirm the update for each image belonging
to the selected case.
