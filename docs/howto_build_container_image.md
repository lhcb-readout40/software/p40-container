## How to build container image

Images are build automatically via the continuous integration tools
from GitLab and pushed in `gitlab-registry`. The process is triggered by
changes in the `p40-lli-device` project.

### by using manual trigger

It is possible to tigger manually the building and storing of the image,
by using the script `scripts/trigger_build_store`.
It requires a `trigger token` defined in the GitLab
[Settings > CI/CD > Pipeline triggers](https://gitlab.cern.ch/lhcb-readout40/software/p40-container/-/settings/ci_cd):

        $ ./scripts/trigger_build_store <token> <image>

### by building images on the local host

* Clone the GitLab project
[p40-container](https://gitlab.cern.ch/lhcb-readout40/software/p40-container)

* To build and image use `make`.
  The `help` argument provides a list of images which can be built:

        $ cd p40-container
        $ make help

* For example, to build the image for the latest `stable` release and to
  store it locally:

        $ cd p40-container
        $ make stable

* To build an image for the `stable` release `6.7.2-0` and to push it in
  `gitlab-registry` (will require to login to `gitlab-registry.cern.ch`):

        $ cd p40-container
        $ make stable release=6.7.2-0 push=go

* From time to time the `push` operation failed. It can be recovered by
  using the script `push-registry.py`

        $ cd p40-container
        $ python3 push-registry.py stable:672-0

* The list of images copied locally, can be seen via basic container command:

        $ podman images

### by generating a trigger from another GitLab project

* Add in  the `.gitlab-ci.yml` file:

        .trigger_p40_container: &trigger_p40_container
            stage: trigger
            variables:
                IMG: $CI_JOB_NAME
            script:
                - "curl -X POST -F token=$CONTAINER_TOKEN -F ref=master -F variables[IMG]=$IMG $CONTAINER_PIPELINE"

  For safety reason the variables `CONTAINER_TOKEN` and `CONTAINER_PIPELINE`
  have to be defined in the GitLab settings > CI/CD > Variables.

