## How to mount local directory in the container

The host directory in which `p40` is launched it mounted in the container
as `/root/outdir`. Files can be exchanged between the host
and the container via this directory.

It is possible to change the host directory by using the option:

        $ p40 start --outdir <path>


It is possible to mount any local directory in the container by using
the p40 option `--mount` or `-m`.

For example, to mount the local directory `~/myprojects/p40-lli-devices`:

        $ p40 start -m ~/myprojects/p40-lli-devices mygit

* The mounted directory is `/root/p40-lli-devices`.

* Path to local directory can be absolute or relative to the one in which
  the command `p40` is launched

* the option `-m` can be repeated to mount several directories.
