### How to build the rpm `lhcb-p40-container`

The RPM is built automatically via the continuous integration tools
from GitLab and pushed in
[https://lbyum.cern.ch/daq40/lli](https://lbyum.cern.ch/daq40/lli)

However it is possible to build the rpm locally:

* Clone the GitLab project
[p40-container](https://gitlab.cern.ch/lhcb-readout40/software/p40-container)

* the RPM `rpmbuild` has to be installed on your machine:

        $ sudo yum install rpmbuild

* Use `make` to build the RPM

        $ cd p40-container
        $ export CI_PROJECT_DIR=`pwd`
        $ make rpm

* The rpm is available in the directory `p40-container/RPM/rpms/x86_64`
